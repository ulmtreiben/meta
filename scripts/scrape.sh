#!/bin/bash
cd $(dirname "$0")/..

source ./config

echo "pull updates"
cd repo/master
git pull origin master

echo "scrape!"
cd ../../scraper
git pull
npm run scrape ../repo $1
cd ../repo/master
git push
